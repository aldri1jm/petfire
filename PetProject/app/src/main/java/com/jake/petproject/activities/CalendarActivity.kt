package com.jake.petproject.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jake.petproject.R
import com.jake.petproject.models.CalendarModel
import com.jake.petproject.models.User
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_calendar.*
import kotlinx.android.synthetic.main.calendar_owner_row.view.*

// Adapter/Firebase/View/Model in part comes from https://www.youtube.com/playlist?list=PL0dzCUj1L5JE-jiBHjxlmXEkQkum_M3R- youtube series
// This database is not currently secure, and it's not recommended to use sensitive information

class CalendarActivity : AppCompatActivity() {

    companion object {
        var currentUser: User? = null
        val USER_KEY = "USER_KEY"
        //global arraylist to be used when populating friends
        var friendsList = ArrayList<String>()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)

        supportActionBar?.title = "Calendar"
        //Clear friendsList to avoid duplicate Friends once the adapter adds again
        friendsList.clear()
        verifyUserIsLoggedIn()
        fetchCurrentUser()
        listenForCalendarEvents()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.calendar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.menu_sign_out -> {
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            R.id.menu_new_event -> {
                val intent = Intent(this, FriendsSelectActivity::class.java)
                // intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            R.id.menu_manage_profile -> {
                val intent = Intent (this, ProfileEditActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun listenForCalendarEvents() {

        val fromId = FirebaseAuth.getInstance().uid
        val reference = FirebaseDatabase.getInstance().getReference("/calendar-events/$fromId")
        val adapter = GroupAdapter<GroupieViewHolder>()

        calendar_recyclerview_calendar.adapter = adapter
        calendar_recyclerview_calendar.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        adapter.clear()

        reference.addChildEventListener(object: ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val calendarEvent = p0.getValue(CalendarModel::class.java) ?: return
                //Fill the calendar with events and listen
                adapter.add(CalendarOwnerItem(calendarEvent))

                adapter.setOnItemClickListener { item, view ->
                    val userItem = item as CalendarOwnerItem
                    val intent = Intent(view.context, CalendarEditActivity::class.java)
                    intent.putExtra(USER_KEY, userItem.event)
                    startActivity(intent)
                }
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
            }
            override fun onChildRemoved(p0: DataSnapshot) {
            }
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }
            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    class CalendarOwnerItem(val event: CalendarModel): Item<GroupieViewHolder>() {
        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            //Fill the recycler view with calendar items
            val ref = FirebaseDatabase.getInstance().getReference("/users/${event.owner}/")
            val refSitter = FirebaseDatabase.getInstance().getReference(("/users/${event.petSitter}/"))

            ref.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    var owner = p0.getValue(User::class.java)


                    Picasso.get().load(owner?.profileImageUrl).into(viewHolder.itemView.calendar_owner_imageview)
                }
                override fun onCancelled(p0: DatabaseError) {
                }
            })

            refSitter.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    var sitter = p0.getValue(User::class.java)

                    Picasso.get().load(sitter?.profileImageUrl).into(viewHolder.itemView.calendar_sitter_imageview)
                }
                override fun onCancelled(p0: DatabaseError) {
                }
            })
            viewHolder.itemView.calendar_owner_status_textview.text = event.status
            viewHolder.itemView.calendar_owner_time_textview.text = event.holdTime
            viewHolder.itemView.calendar_owner_date_textview.text = event.holdDate

        }
        override fun getLayout(): Int {
            return R.layout.calendar_owner_row
        }
    }


    private fun verifyUserIsLoggedIn() {
        val uid = FirebaseAuth.getInstance().uid
        if (uid === null) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

    }

    private fun fetchCurrentUser() {
        val uid = FirebaseAuth.getInstance().uid
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
        val refer = ref.child("friends")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                currentUser = p0.getValue(User::class.java)
            }
            override fun onCancelled(p0: DatabaseError) {
            }
        }
        )
        refer.addListenerForSingleValueEvent(object : ValueEventListener {
            var counter = 0

            override fun onDataChange(p0: DataSnapshot) {
                for(ds in p0.children) {
                    //Loop through the friends node for the user in Firebase and add friend uid's to friendsList ArrayList
                    counter++
                    val stringHolder = ds.getValue(String::class.java)
                    friendsList.add(stringHolder!!)
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }
}
