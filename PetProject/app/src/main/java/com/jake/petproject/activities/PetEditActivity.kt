package com.jake.petproject.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.jake.petproject.R
import com.jake.petproject.activities.CalendarActivity.Companion.currentUser
import com.jake.petproject.models.PetModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_pet_edit.*
import java.util.*

class PetEditActivity : AppCompatActivity() {


    var petUrl: String=""
    var petKey: String=""
    //Set photoChange to avoid trying to submit without changing image crashes
    var photoChange: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pet_edit)
        photo_select_button_profile_edit.alpha = 0f


        fillActivity()

        photo_select_button_profile_edit.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.pet_edit_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_save -> {
                if (photoChange) {
                    uploadImage()
                }
                else {
                    var currentPet = intent.getParcelableExtra<PetModel>(ProfileEditActivity.USER_KEY)
                    updatePet(currentPet.petImageUrl)
                }
            }
            R.id.menu_cancel -> {
                finish()
            }

        }
        return super.onOptionsItemSelected(item)
    }
    var selectedPhotoUri: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {

            selectedPhotoUri = data.data
            photoChange = true

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            pet_view_image_view.setImageBitmap(bitmap)

        }
    }

    private fun fillActivity() {
        var currentPet = intent.getParcelableExtra<PetModel>(ProfileEditActivity.USER_KEY)
        supportActionBar?.title = currentPet.petName

        petKey  = currentPet.uuid
        petUrl = currentPet.petImageUrl

        Picasso.get().load(currentPet.petImageUrl).into(pet_view_image_view)
        pet_edit_name_edittext.setText(currentPet.petName)
        pet_edit_breed_edittext.setText(currentPet.petBreed)
        pet_view_owner_textview.text = currentUser?.username
        pet_edit_feeding_edittext.setText(currentPet.petFeed)
        pet_edit_cleaning_edittext.setText(currentPet.petCleanUp)
        pet_edit_walking_edittext.setText(currentPet.petWalk)
        pet_edit_notes_edittext.setText(currentPet.petNotes)

    }


    private fun uploadImage() {
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    petUrl = it.toString()
                    updatePet(petUrl)

                }
            }
    }

    private fun updatePet(petUro: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/pets/$uid/$petKey")

        val petName = pet_edit_name_edittext.text.toString()
        val petBreed = pet_edit_breed_edittext.text.toString()
        val petType = pet_edit_spinner.selectedItem.toString()
        val petFeed = pet_edit_feeding_edittext.text.toString()
        val petWalk = pet_edit_walking_edittext.text.toString()
        val petCleanUp = pet_edit_cleaning_edittext.text.toString()
        val petNotes = pet_edit_notes_edittext.text.toString()

        val editPet = PetModel(uid, petName, petUro, petBreed, petType, petFeed, petWalk, petNotes, petCleanUp, petKey)

        ref.setValue(editPet).addOnSuccessListener {
            val intent = Intent(this, ProfileEditActivity::class.java)

            startActivity(intent)
        }

    }
}
