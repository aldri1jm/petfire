package com.jake.petproject.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class PetModel(val uid: String, val petName: String, val petImageUrl: String, val petBreed: String, val petType: String, val petFeed: String, val petWalk: String, val petNotes: String, val petCleanUp: String, val uuid: String): Parcelable {
    constructor() : this("","","", "", "", "", "", "", "", "")
}