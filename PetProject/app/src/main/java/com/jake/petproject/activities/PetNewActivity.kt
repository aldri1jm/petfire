package com.jake.petproject.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.jake.petproject.R
import com.jake.petproject.activities.CalendarActivity.Companion.currentUser
import com.jake.petproject.models.PetModel
import kotlinx.android.synthetic.main.activity_pet_new.*
import java.util.*

class PetNewActivity : AppCompatActivity() {
    var photoChange: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pet_new)

        supportActionBar?.title = "New Pet"
        pet_view_owner_textview.text = currentUser?.username

        photo_select_button_profile_edit.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            //set the intent to upload to the images storage in firebase
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.pet_edit_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_save -> {
                if (photoChange) {
                    Toast.makeText(this, "Please wait while your pet is being created", Toast.LENGTH_LONG).show()
                    performAddPet()
                }
                else {
                    Toast.makeText(this, "Failed to create pet, please select a photo", Toast.LENGTH_LONG).show()
                }

            }
            R.id.menu_cancel -> {
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    var selectedPhotoUri: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            photoChange = true
            selectedPhotoUri = data.data

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)
            pet_view_image_view.setImageBitmap(bitmap)
            photo_select_button_profile_edit.alpha = 0f
        }
    }

    private fun performAddPet() {

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    //Utilize onSuccessListener as method could complete while the bitmap was still being processed/uploaded
                    submitPet(it.toString())
                }
            }
    }

    private fun submitPet(petImageUrl: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/pets/$uid").push()
        val petName = pet_new_name_edittext.text.toString()
        val petBreed = pet_new_breed_edittext.text.toString()
        val petType = pet_new_spinner.selectedItem.toString()
        val petFeed = pet_new_feeding_edittext.text.toString()
        val petWalk = pet_new_walking_edittext.text.toString()
        val petCleanUp = pet_new_cleaning_edittext.text.toString()
        val petNotes = pet_new_notes_edittext.text.toString()

        val key = ref.key
        val newPet = PetModel(uid, petName, petImageUrl, petBreed, petType, petFeed, petWalk, petNotes, petCleanUp, key!!)

            ref.setValue(newPet)
                .addOnSuccessListener {
                    val intent = Intent(this, ProfileEditActivity::class.java)
                    // intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                }
    }
}
