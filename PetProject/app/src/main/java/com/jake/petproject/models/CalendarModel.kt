package com.jake.petproject.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CalendarModel (var holdDate: String ="", var holdTime: String="", var petSitter: String="", var owner: String="",var status: String="Pending", var partnerKey: String="", var uuid: String="") : Parcelable {
}