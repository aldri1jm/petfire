package com.jake.petproject.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User(val uid: String, val username: String, var profileImageUrl: String, var address: String, var phone: String): Parcelable {
    constructor() : this("","","", "","")
}