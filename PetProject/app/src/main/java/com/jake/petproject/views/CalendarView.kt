package com.jake.petproject.views

import android.util.Log
import com.jake.petproject.R
import com.jake.petproject.models.CalendarModel
import com.jake.petproject.models.User
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.calendar_owner_row.view.*
import java.text.FieldPosition

class CalendarView(val calendarEvent: CalendarModel): Item<GroupieViewHolder>() {
    var owner: User? = null

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.calendar_owner_date_textview.text = calendarEvent.holdDate
        viewHolder.itemView.calendar_owner_time_textview.text = calendarEvent.holdTime
        
    }
    override fun getLayout(): Int {
        return R.layout.calendar_owner_row
    }
}