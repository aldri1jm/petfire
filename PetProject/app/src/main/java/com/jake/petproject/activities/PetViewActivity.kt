package com.jake.petproject.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.jake.petproject.R
import com.jake.petproject.models.PetModel
import com.jake.petproject.models.User
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_pet_view.*

class PetViewActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pet_view)

        fillActivity()

    }

    private fun fillActivity() {
        var currentPet = intent.getParcelableExtra<PetModel>(ProfileEditActivity.USER_KEY)
        val reference = FirebaseDatabase.getInstance().getReference("/users/${currentPet.uid}")
        supportActionBar?.title = currentPet.petName
        Picasso.get().load(currentPet.petImageUrl).into(pet_view_image_view)
        pet_view_name_textview.text = currentPet.petName
        pet_view_type_textview.text = currentPet.petType
        pet_view_breed_textview.text = currentPet.petBreed
        pet_view_feeding_textview.text = currentPet.petFeed
        pet_view_walking_textview.text = currentPet.petWalk
        pet_view_cleaning_textview.text = currentPet.petCleanUp
        pet_view_notes_textview.text = currentPet.petNotes

        reference.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                val owner = p0.getValue(User::class.java)
                pet_view_owner_textview.text = owner?.username
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })

    }

}
