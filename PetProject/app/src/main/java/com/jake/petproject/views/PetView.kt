package com.jake.petproject.views

import android.util.Log
import com.jake.petproject.R
import com.jake.petproject.models.PetModel
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.pet_row_populate.view.*

class PetView(val pet: PetModel): Item<GroupieViewHolder>() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        Picasso.get().load(pet.petImageUrl).into(viewHolder.itemView.pet_row_imageview)
    }
    override fun getLayout(): Int {
        return R.layout.pet_row_populate
    }
}
