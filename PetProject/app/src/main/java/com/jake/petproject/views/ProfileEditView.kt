package com.jake.petproject.views

import com.jake.petproject.R
import com.jake.petproject.models.User
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class ProfileEditView(val user: User): Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
    }
    override fun getLayout(): Int
    {
        return R.layout.pet_row_populate
    }
    }