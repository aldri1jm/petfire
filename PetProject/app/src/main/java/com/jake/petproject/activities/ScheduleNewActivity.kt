package com.jake.petproject.activities

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TimePicker
import android.widget.Toast
import com.afollestad.date.dayOfMonth
import com.afollestad.date.month
import com.afollestad.date.year
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.datetime.datePicker
import com.afollestad.materialdialogs.datetime.timePicker
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.jake.petproject.R
import com.jake.petproject.activities.CalendarActivity.Companion.currentUser
import com.jake.petproject.models.CalendarModel
import com.jake.petproject.models.User
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_schedule_new.*
import java.text.SimpleDateFormat
import java.util.*

//Material dialogue information available at https://github.com/afollestad/material-dialogs/blob/master/documentation/DATETIME.md

class ScheduleNewActivity : AppCompatActivity() {

    var calendarNew = CalendarModel()

    var sitter: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_new)

        supportActionBar?.title = "New Event"

        //Grab the passed sitter data inorder to load their picture
        sitter = intent.getParcelableExtra<User>(FriendsSelectActivity.USER_KEY)

        fillActivity()


        set_date_button.setOnClickListener {
            //Grab the Date and set the date textfield with the selected value
            MaterialDialog(this).show {
                datePicker { dialog, date ->
                    var rDate: String = "holder"
                    val d = date.dayOfMonth.toString()
                    val mRaw = date.month
                    var m: String=""
                    val y = date.year.toString()

                    when (mRaw) {
                        0 -> m = "Jan"
                        1 -> m = "Feb"
                        2 -> m = "Mar"
                        3 -> m = "Apr"
                        4 -> m = "May"
                        5 -> m = "June"
                        6 -> m = "July"
                        7 -> m = "Aug"
                        8 -> m = "Sept"
                        9 -> m = "Oct"
                        10 -> m = "Nov"
                        11 -> m = "Dec"
                    }
                    rDate = d + " " + m + ", " + y
                    setDateText(rDate)
                }

            }
        }

        set_time_button.setOnClickListener {
            //Grab the Time and set the time textfield with the selected value
            MaterialDialog (this).show {
                timePicker { dialog, time ->
                    val t = time.get(Calendar.HOUR).toString()
                    val mm: String
                    if (time.get(Calendar.MINUTE) < 10)  {
                        mm = "0" + time.get(Calendar.MINUTE).toString()
                    }
                    else {
                        mm = time.get(Calendar.MINUTE).toString()
                    }

                    val timeHoldText = t +":"+mm

                    setTimeText(timeHoldText)
                }
            }
        }

    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.calendar_edit_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_submit -> {
                if (calendarNew.holdDate != "" || calendarNew.holdTime != "") {
                        performSchedule()
                    }
                }
            R.id.menu_cancel -> {
                finish()
            }

             }
        return super.onOptionsItemSelected(item)
    }

    private fun setDateText(newDate: String) {
        date_textview_schedule.text = newDate
        calendarNew.holdDate = newDate
    }
    private fun setTimeText(newTime: String) {
        time_textview_schedule.text = newTime
        calendarNew.holdTime = newTime
    }
    private fun performSchedule() {

        val dateSchedule = calendarNew.holdDate
        val timeSchedule = calendarNew.holdTime

        val fromId = FirebaseAuth.getInstance().uid

        //Create two nodes in Firebase for the event, one under each user and make sure that the key for the other event is included for easy access
        val reference = FirebaseDatabase.getInstance().getReference("/calendar-events/$fromId/").push()
        val reference2 = FirebaseDatabase.getInstance().getReference("/calendar-events/${sitter!!.uid}/").push()

        val ownerKey = reference.key
        val sitterKey = reference2.key


        val calendarEventOwner = CalendarModel(dateSchedule, timeSchedule, sitter!!.uid, fromId!!, "Pending", sitterKey!!, ownerKey!!)
        val calendarEventSitter = CalendarModel(dateSchedule, timeSchedule, sitter!!.uid, fromId!!, "Pending", ownerKey!!, sitterKey)

        reference.setValue(calendarEventOwner)
           .addOnSuccessListener {
            }
        reference2.setValue(calendarEventSitter)
            .addOnSuccessListener {
            }
        finish()
    }

    private fun fillActivity() {
        username_textview_schedule.text = sitter?.username
        Picasso.get().load(sitter?.profileImageUrl).into(sitter_imageview_schedule)
    }

}
