package com.jake.petproject.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.jake.petproject.R
import com.jake.petproject.activities.CalendarActivity.Companion.currentUser
import com.jake.petproject.models.CalendarModel
import com.jake.petproject.models.PetModel
import com.jake.petproject.models.User
import com.jake.petproject.views.PetView
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_calendar_edit.*

class CalendarEditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar_edit)

        fillActivity()

    }

    private fun fillActivity() {
        var currentEvent = intent.getParcelableExtra<CalendarModel>(CalendarActivity.USER_KEY)
        supportActionBar?.title = currentEvent.holdDate + " " + currentEvent.holdTime

        val referenceOwner = FirebaseDatabase.getInstance().getReference ("/users/${currentEvent.owner}/")
        val referenceSitter = FirebaseDatabase.getInstance().getReference("/users/${currentEvent.petSitter}")

        val referencePets = FirebaseDatabase.getInstance().getReference("/pets/${currentUser?.uid}")
        date_calendar_edit_textview.text = currentEvent.holdDate
        time_calendar_edit_textview.text = currentEvent.holdTime

        val adapter = GroupAdapter<GroupieViewHolder>()
        calendar_edit_pets_recyclerview.adapter = adapter

        referenceOwner.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                var owner = p0.getValue(User::class.java)

                addresscalendar_edit_textview.text = owner?.address

                calendar_edit_owner_username_textview.text = owner?.username
                calendar_edit_owner_phone_textview.text = owner?.phone
                Picasso.get().load(owner?.profileImageUrl).into(pet_owner_imageview_calendar_edit)
            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })

        referenceSitter.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                var sitter = p0.getValue(User::class.java)

                calendar_edit_sitter_username_textview.text = sitter?.username
                calendar_edit_sitter_phone_textview.text = sitter?.phone

                Picasso.get().load(sitter?.profileImageUrl).into(pet_sitter_imageview_calendar_edit)
            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })

        referencePets.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    val pet = it.getValue(PetModel::class.java)
                    adapter.add(PetView(pet!!))

                }
                adapter.setOnItemClickListener { item, view ->
                    val petMan = item as PetView
                    val intent = Intent(view.context, PetViewActivity::class.java)
                    intent.putExtra(ProfileEditActivity.USER_KEY, petMan.pet)
                    startActivity(intent)
                    finish()
                }
            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })


    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.calendar_status_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {

            R.id.menu_decline -> {
                //modify the events for both users
                var currentEvent = intent.getParcelableExtra<CalendarModel>(CalendarActivity.USER_KEY)
                val referenceCalOwner = FirebaseDatabase.getInstance().getReference ("/calendar-events/${currentEvent.owner}/${currentEvent.uuid}")
                val referenceCalSitter = FirebaseDatabase.getInstance().getReference("/calendar-events/${currentEvent.petSitter}/${currentEvent.partnerKey}")
                currentEvent.status = "Declined"

                referenceCalOwner.setValue(currentEvent)
                referenceCalSitter.setValue(currentEvent)
                val intent = Intent(this, CalendarActivity::class.java)
                startActivity(intent)


            }
            R.id.menu_accept -> {
                var currentEvent = intent.getParcelableExtra<CalendarModel>(CalendarActivity.USER_KEY)
                val referenceCalOwner = FirebaseDatabase.getInstance().getReference ("/calendar-events/${currentEvent.owner}/${currentEvent.uuid}")
                val referenceCalSitter = FirebaseDatabase.getInstance().getReference("/calendar-events/${currentEvent.petSitter}/${currentEvent.partnerKey}")
                currentEvent.status = "Accepted"

                referenceCalOwner.setValue(currentEvent)
                referenceCalSitter.setValue(currentEvent)
                val intent = Intent(this, CalendarActivity::class.java)
                startActivity(intent)

            }
        }
        return super.onOptionsItemSelected(item)
    }
}
