package com.jake.petproject.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.jake.petproject.R
import com.jake.petproject.activities.CalendarActivity.Companion.currentUser
import com.jake.petproject.models.PetModel
import com.jake.petproject.views.PetView
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_profile_edit.*
import java.util.*

class ProfileEditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit)

        supportActionBar?.title = "Edit Profile"

        photo_select_button_profile_edit.alpha = 0f

        setupUser()

        photo_select_button_profile_edit.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }


    }

    companion object {
        val USER_KEY = "USER_KEY"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.profile_edit_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
    var selectedPhotoUri: Uri? = null
    var photoChange: Boolean = false

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            photoChange = true
            selectedPhotoUri = data.data

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            profile_edit_imageview.setImageBitmap(bitmap)
        }
    }


    private fun setupUser() {
        Picasso.get().load(currentUser?.profileImageUrl).into(profile_edit_imageview)
        profile_edit_username_textview.text = currentUser?.username

        profile_edit_address_edittext.setText(currentUser?.address)
        profile_edit_phone_edittext.setText(currentUser?.phone)

        val reference = FirebaseDatabase.getInstance().getReference("/pets/${currentUser?.uid}")
        val adapter = GroupAdapter<GroupieViewHolder>()
        profile_edit_pets_recyclerview.adapter = adapter

        reference.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    val pet = it.getValue(PetModel::class.java)
                    adapter.add(PetView(pet!!))
                }
                adapter.setOnItemClickListener { item, view ->
                    //Load the petview layout/activity to see uneditable pet details for both sitter and owner
                    val petMan = item as PetView
                    val intent = Intent(view.context, PetEditActivity::class.java)
                    intent.putExtra(USER_KEY, petMan.pet)
                    startActivity(intent)
                    finish()
                }
            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }


    private fun uploadImage() {
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {

                    currentUser?.profileImageUrl = it.toString()

                    setResults(currentUser?.profileImageUrl!!)

                }
            }
    }

    private fun setResults(profileUrl: String) {
        val referenceAddress = FirebaseDatabase.getInstance().getReference("users/${currentUser?.uid}/address")
        val referencePhone = FirebaseDatabase.getInstance().getReference("users/${currentUser?.uid}/phone")
        val referenceUrl = FirebaseDatabase.getInstance().getReference("users/${currentUser?.uid}/profileImageUrl")

        referenceAddress.setValue(profile_edit_address_edittext.text.toString())
        referencePhone.setValue(profile_edit_phone_edittext.text.toString())
        referenceUrl.setValue(profileUrl).addOnSuccessListener {
            val intent = Intent(this, CalendarActivity::class.java)
               startActivity(intent)
               finish()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.menu_add_pet -> {
                val intent = Intent(this, PetNewActivity::class.java)
                startActivity(intent)
            }

            R.id.menu_save -> {
                if (photoChange == true) {
                    uploadImage()
                }
                else {
                    //pass the current profileImageUrl if there was no new image was uploaded
                    setResults(currentUser?.profileImageUrl!!)
                }
               //

            }

        }
        return super.onOptionsItemSelected(item)
    }
}
