package com.jake.petproject.views

import android.util.Log
import com.jake.petproject.R
import com.jake.petproject.models.User
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.friends_row_populate.view.*

class UserItem(val user: User): Item<GroupieViewHolder>() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.username_textview_populate.text = user.username
        Picasso.get().load(user.profileImageUrl).into(viewHolder.itemView.friend_row_imageview)
    }
    override fun getLayout(): Int {
        return R.layout.friends_row_populate
    }
}
