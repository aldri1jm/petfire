package com.jake.petproject.activities

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DividerItemDecoration
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.jake.petproject.R
import com.jake.petproject.activities.CalendarActivity.Companion.currentUser
import com.jake.petproject.activities.CalendarActivity.Companion.friendsList
import com.jake.petproject.models.User
import com.jake.petproject.views.UserItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_friends_select.*

class FriendsSelectActivity : AppCompatActivity() {

    var stringHolder: String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends_select)

        supportActionBar?.title = "Select Sitter"
        fetchUsers()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.friends_list_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }



    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_add_sitter -> {
                showFilterDialog()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        val USER_KEY = "USER_KEY"
    }

    private fun fetchUsers() {
        val adapter = GroupAdapter<GroupieViewHolder>()
        friend_select_recyclerview.adapter = adapter
        friend_select_recyclerview.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        val reference = FirebaseDatabase.getInstance().getReference("/users")
        reference.addListenerForSingleValueEvent(object: ValueEventListener {

            override fun onDataChange(p0: DataSnapshot) {

                p0.children.forEach {
                    val user = it.getValue(User::class.java)
                    friendsList.forEach {
                        if (it == user?.uid) {
                            //Loop through the friends list to match uid's to populate friends/sitters
                            adapter.add(UserItem(user))
                        }
                    }
                }
                //Listen for when a sitter is clicked to schedule a new event
                adapter.setOnItemClickListener { item, view ->
                    val userItem = item as UserItem
                    val intent = Intent(view.context, ScheduleNewActivity::class.java)
                    intent.putExtra(USER_KEY, userItem.user)
                    startActivity(intent)
                    finish()
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }

    private fun addFriend(newFriend: String) {
        //Attempt to add a friend to the friends list and the friend node in Firebase to be populated by the adapter
        val reference = FirebaseDatabase.getInstance().getReference("/users")
        val friendRef = FirebaseDatabase.getInstance().getReference("/users/${currentUser?.uid}/friends/").push()
        reference.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    val user = it.getValue(User::class.java)
                    if (user?.username ==newFriend ) {
                        //Verify that the friend being added is not already on the friends list and that it isn't the current user
                        if (user.uid !in friendsList && user.uid != currentUser?.uid ) {
                            friendRef.setValue(user.uid)
                            friendsList.add(user.uid!!)
                        }
                    }
                }
                }
            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }

    private fun showFilterDialog() {
        val dialog = MaterialDialog(this)
            .noAutoDismiss()
            .customView(R.layout.activity_add_friend)

        dialog.show {
            positiveButton(R.string.add) { dialog ->
                //Grab the username from the dialogue prompt
                stringHolder = dialog.findViewById<EditText>(R.id.username_add_friend_textview).text.toString()
                dialog.dismiss()
                //Attempt to add the friend
                addFriend(stringHolder)
                fetchUsers()
            }
            negativeButton(R.string.cancel) { dialog ->
                dialog.dismiss()
            }
        }
    }

}
